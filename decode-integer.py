#!/usr/bin/env python

import argparse
import sys

# decode-integer.py <value> <prefix>
# hpack-coding.py -ei <value> <prefix> - encode integer
# hpack-coding.py -di <value> <prefix> - decode integer
# hpack-coding.py -es <string> - encode string as huffman
# hpack-coding.py -ds <string> - decode huffman to string
# hpack-coding.py -eh <header> [<header> ...] - encode ordered headers
# hpack-coding.py -dh <string> - decode encoded headers

def decode_integer(value, prefix):
    accum = 0
    offset = 0

    if prefix > 0:
        mask = ((1 << prefix) - 1) & 0xffffffff
        accum = int(value[offset], 16) & mask
        offset += 1

        if accum != mask:
            return accum

    factor = 1
    if offset > len(value):
        raise Exception('Ran out of data to decode integer')

    b = int(value[offset], 16)
    has_chain = bool(b & 0x80)
    accum += (b & 0x7f) * factor

    offset += 1
    factor *= 128

    while has_chain:
        if accum >= 0x800000:
            raise Exception('Decoding integer >= 0x800000')

        if offset > len(value):
            raise Exception('Ran out of data to decode integer')

        b = int(value[offset], 16)
        has_chain = bool(b & 0x80)
        accum += (b & 0x7f) * factor

        offset += 1
        factor *= 128

    return accum

value = sys.argv[1].split()
prefix = int(sys.argv[2])
decoded_value = decode_integer(value, prefix)
print 'Decoded value is %d' % decoded_value


def encode_integer(value, prefix):
    mask = ((1 << prefix) - 1) & 0xffffffff
    if value < mask:
        return '%02x' % (value,)

    res = []
    if mask:
        value -= mask
        res.append('%02x' % (mask,))

    q = 1
    r = 1
    while q:
        q = value / 128
        r = value % 128
        if q:
            r |= 0x80
        value = q
        res.append('%02x' % (r,))

    return res


def huffman_encode(string):
    raise NotImplementedError


def huffman_decode(hstring):
    raise NotImplementedError
