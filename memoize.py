class MemoizedFib(object):
    def __init__(self):
        self.fib = [1, 1]

    def __call__(self, i):
        i = int(i)
        if i < 0:
            raise ValueError

        while len(self.fib) <= i:
            self.fib.append(self.fib[-1] + self.fib[-2])
        return self.fib[i]

    def __getitem__(self, i):
        return self(i)

class MemoizedPascal(object):
    def __init__(self):
        self.pascal = [[1]]

    def __call__(self, i):
        i = int(i)
        if i < 0:
            raise ValueError

        while len(self.pascal) <= i:
            o = self.pascal[-1]
            l = [1]
            l.extend([o[j] + o[j + 1] for j in xrange(len(o) - 1)])
            l.append(1)
            self.pascal.append(l)

        return self.pascal[i]

    def __getitem__(self, i):
        return self(i)
