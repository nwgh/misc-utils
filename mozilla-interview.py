#!/usr/bin/env python

import sys
import os
import shutil

RESOLVCONF='/Users/hurley/resolv.conf'

def fix_resolvconf():
    bakfile = '%s.bak' % (RESOLVCONF,)
    tmpfile = '%s.tmp' % (RESOLVCONF,)
    replaced = False
    f = file(RESOLVCONF)
    b = file(bakfile, 'w')
    t = file(tmpfile, 'w')
    for line in f:
        b.write(line)
        if line.startswith('nameserver ') and not replaced:
            t.write('nameserver 127.0.0.1\n')
            replaced = True
        else:
            t.write(line)
    f.close()
    b.close()
    t.close()
    shutil.copyfile(tmpfile, RESOLVCONF)
    os.unlink(tmpfile)

if __name__ == '__main__':
    fix_resolvconf()
    sys.exit(0)
