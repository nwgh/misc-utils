import copy
import math
import struct

R = [7, 12, 17, 22, 7, 12, 17, 22, 7, 12, 17, 22, 7, 12, 17, 22,
     5, 9, 14, 20, 5, 9, 14, 20, 5, 9, 14, 20, 5, 9, 14, 20,
     4, 11, 16, 23, 4, 11, 16, 23, 4, 11, 16, 23, 4, 11, 16, 23,
     6, 10, 15, 21, 6, 10, 15, 21, 6, 10, 15, 21, 6, 10, 15, 21]
K = [int(math.floor(abs(math.sin(i + 1.0)) * (2 ** 32))) for i in range(64)]
H = [0x67452301, 0xefcdab89, 0x98badcfe, 0x10325476]

def rll(x, c):
    return ((x << c) & 0xffffffff) | (x >> (32 - c))

def md5(msg):
    h = copy.deepcopy(H) # Don't blow away our starting constants

    data = bytes(msg) # Just in case we get utf8 or some shit
    mlen = len(data) * 8 # length of message in bits
    mod = mlen % 512
    if mod >= 448:
        # Need to wrap around appropriately
        mod -= 512
    plen = 448 - mod # How many bits we need to use as padding
    dlen = mlen + plen + 64 # Total length of data for the hash loop
    plen_bytes = (plen / 8) - 1 # First pad byte is special

    # Format of data for md5 hash is:
    #  *msg as passed to this function
    #  *one byte with the high-order bit set to 1
    #  *0 or more nul bytes to make the length in bits = 448 mod 512
    #  *little-endian 64bit integer holding length in bits of unpadded msg
    fmt = '<%dsB%sQ' % (len(data), 'x' * plen_bytes)
    data = struct.pack(fmt, data, 1 << 7, mlen)

    nchunks = dlen / 512
    chunks = [data[i * 64:(i + 1) * 64] for i in range(nchunks)]
    for chunk in chunks:
        words = struct.unpack('<IIIIIIIIIIIIIIII', chunk)
        a, b, c, d = h

        # Calculate the hash update for this chunk
        for i in range(64):
            if i < 16:
                f = (b & c) | ((~b) & d)
                g = i
            elif i < 32:
                f = (d & b) | ((~d) & c)
                g = (5 * i + 1) % 16
            elif i < 48:
                f = b ^ c ^ d
                g = (3 * i + 5) % 16
            else:
                f = c ^ (b | (~d))
                g = (7 * i) % 16

            x = (a + f + K[i] + words[g]) & 0xffffffff
            a, b, c, d = d, (b + rll(x, R[i])) & 0xffffffff, b, c

        # Update hash with current chunk values
        h[0] = (h[0] + a) & 0xffffffff
        h[1] = (h[1] + b) & 0xffffffff
        h[2] = (h[2] + c) & 0xffffffff
        h[3] = (h[3] + d) & 0xffffffff

    sum_ = struct.pack('<IIII', *h)
    return ''.join(['%02x' % b for b in struct.unpack('B' * 16, sum_)])
