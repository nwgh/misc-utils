#!/usr/bin/env python

# Originally had a[1,10,20,28,64] b[2,7,29,62] c[4,26,30,61,63,70]
# Also had a[1,10,20,29,64] b[2,7,28,62] c[4,26,30,61,63,70]
# Also had a[2,10,20,29,64] b[1,7,28,62] c[4,26,30,61,63,70]
wordset = {'a':[2, 10, 20, 29, 64], 'b':[1, 7, 28, 62], 'c':[4, 26, 61, 70]}
delta = -1
span = (None, None)

while True:
    print 'Wordset is %s' % str(wordset)
    min_k = min(wordset, key=wordset.get)
    max_p = max(wordset, key=wordset.get)
    min_v = wordset[min_k][0]
    max_v = wordset[max_p][0]
    print 'min_k = %s, max_p = %s, min_v = %s, max_v = %s' % (min_k, max_p, min_v, max_v)

    if (max_v - min_v + 1) < delta or delta < 0:
        delta = max_v - min_v + 1
        span = (min_v, max_v)

    if len(wordset[min_k]) > 1:
        wordset[min_k].pop(0)
    else:
        break

    if delta == len(wordset):
        break
    print ''

print ''
if delta:
    print 'Delta is %s (%s, %s)' % (delta, span[0], span[1])
else:
    print 'Search terms not found'
